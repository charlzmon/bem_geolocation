const Redis = require('ioredis');
const redis = new Redis();
const bemHash = process.env.BEM_HASH_MAP || "bemStateHash";
const timeoutHash = process.env.TIMEOUT_HASH_MAP || "timeoutHash";

let bems = [
    { "000071ac-3600-97be-0430-4100454e6034": JSON.stringify({ fwversion: "3.0.9", "status": "connected", "last_up": "2020-01-12T12:25:08Z", "lon": 11.11, "lat": 11.11 }) },
    { "000071ac-4200-97be-0430-1900454e6034": JSON.stringify({ fwversion: "3.1.4", "status": "connected", "last_up": "2020-01-11T15:25:08Z", "lon": 24.24, "lat": 24.24 }) },
    
    { "0000f1b6-5800-e9b2-0130-3800454e6034": JSON.stringify({ "status": "disconnected", "last_up": "2020-01-01T12:25:08Z", "loc_await": false, "lon": 11.11, "lat": 11.11 }) },
   /* { "0000f1b6-4600-e9b2-0130-2300454e6034": JSON.stringify({ "status": "disconnected", "last_up": "2020-01-13T12:25:08Z", "loc_await": false, "lon": 24.24, "lat": 24.24 }) },
    { "000071ac-3b00-97be-0430-1000454e6034": JSON.stringify({ "status": "disconnected", "last_up": "2020-01-15T14:25:08Z", "loc_await": false, "lon": 24.24, "lat": 24.24 }) },
    { "000071ac-3600-97be-0430-2f00454e6034": JSON.stringify({ "status": "disconnected", "last_up": "2020-01-10T12:25:08Z", "loc_await": false, "lon": 11.11, "lat": 11.11 }) },
    { "000071ac-4000-97be-0430-4b00454e6034": JSON.stringify({ "status": "disconnected", "last_up": "2019-11-15T12:25:08Z", "loc_await": false, "lon": 11.11, "lat": 11.11 }) },
    { "0000f1b6-3700-e9b2-0130-5300454e6034": JSON.stringify({ "status": "disconnected", "last_up": "2019-05-15T12:25:08Z", "loc_await": false, "lon": 24.24, "lat": 24.24 }) },
    { "000071ac-3600-97be-0430-2900454e6034": JSON.stringify({ "status": "disconnected", "last_up": "2019-01-15T12:25:08Z", "loc_await": false, "lon": 24.24, "lat": 24.24 }) }
*/
];

let timeouts = [
    { "000071ac-3600-97be-0430-4100454e6034": 0.016 },
    { "000071ac-4200-97be-0430-1900454e6034": 1 },
    { "0000f1b6-5800-e9b2-0130-3800454e6034": 1 },
    { "0000f1b6-4600-e9b2-0130-2300454e6034": 24 },
    { "000071ac-3b00-97be-0430-1000454e6034": 24 },
    { "000071ac-3600-97be-0430-2f00454e6034": 1 },
    { "000071ac-4000-97be-0430-4b00454e6034": 1 },
    { "0000f1b6-3700-e9b2-0130-5300454e6034": 24 },
    { "000071ac-3600-97be-0430-2900454e6034": 24 }
];


async function bemmer() {
    for (i = 0; i < bems.length; i++) {
        redis.hmset(bemHash, bems[i], (err, reply) => {
            if (err) console.error(err);
            else console.log(reply);
        });
    }
}


async function timer() {
    for (i = 0; i < timeouts.length; i++) {
        redis.hmset(timeoutHash, timeouts[i], (err, reply) => {
            if (err) console.error(err);
            else console.log(reply);
        });
    }
}

bemmer();
timer();

setTimeout(function () { redis.disconnect(); }, 1000);