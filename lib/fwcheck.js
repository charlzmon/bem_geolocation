var MongoClient = require('mongodb').MongoClient;
var url = 'mongodb://localhost:27019';

var checked = [];

MongoClient.connect(url, function (err, client) {
  if (err) throw err;

  var db = client.db('bem-mgmt');
  var col = db.collection('fw_update_config').find(
    {
      $or: [
        { fwVersion: "3.0.9" },
        { fwVersion: "3.0.10" },
        { fwVersion: "3.1.0" },
        { fwVersion: "3.1.1" },
        { fwVersion: "3.1.2" },
        { fwVersion: "3.1.3" },
        { fwVersion: "3.1.4" },
        { fwVersion: "3.1.5" }
      ]
    });

  col.forEach(function (item) {
    checked.push(item.UID);
  });
});

function checkUID() {
  setTimeout(() => {
    return checked;
  }, 5000)
}


