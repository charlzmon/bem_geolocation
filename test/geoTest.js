const assert = require('chai').assert;
const geo = require('../locationApp');

const Redis = require("./../lib/redisHelper");
const IOredis = require('ioredis');
const moment = require('moment');

//Misc
moment().format();
let log = console.log;

//Redis variables
const inQueueName = process.env.LOCATION_EVENT_QUEUE || "locationQueue";
const outQueueName = process.env.ROUTER_OUT_QUEUE || "locationQueue";
const bemHash = process.env.BEM_HASH_MAP || "bemStateHash";
const timeoutHash = process.env.TIMEOUT_HASH_MAP || "timeoutHash";
//Redis instances
const redisPort = process.env.REDIS_PORT || 6379;
const redisHost = process.env.REDIS_HOST || "localhost";
const pushCli = new Redis(redisPort, redisHost);
const lisCli = new Redis(redisPort, redisHost);
const redis = new IOredis();


function quePusher(message) {
    for (let i = 0; i < message.length; i += 1) {
        pushCli.queueMessage(outQueueName, message[i], function () {
            log('queued');
        });
    }
}

uid_1 = '000071ac-3600-97be-0430-4100454e6034';
uid_2 = '000071ac-4200-97be-0430-1900454e6034';

async function getHash(key, UID) {
    try {
        let reply = await redis.hmget(key, UID);
        let parsed = JSON.parse(reply[0]);
        return parsed;
    } catch{ print(err); }
}

describe('getSWV', async () => {

    it('Adds bems', async () => {
        let getswv_message = [
            "{\"UID\":\"000071ac-3600-97be-0430-4100454e6034\",\"type\":\"getSwVers\",\"enc\":\"base64\",\"payload\":\"UQoAAQYJAAADBAMACcI=\",\"at\":1579783035226,\"parseOk\":true,\"parseResultMessage\":\"Parsed OK\",\"payloadLength\":8,\"flags\":1,\"isReply\":true,\"hardware_revision\":9,\"hardware_revision_MSB\":0,\"hardware_variant\":0,\"hardware_variant_options\":3,\"sw_version_class\":4,\"sw_version_major\":3,\"sw_version_minor\":0,\"sw_version_patch\":9,\"swVersion\":\"3.0.9\",\"receiveAt\":1579783035226}",
            "{\"UID\":\"000071ac-4200-97be-0430-1900454e6034\",\"type\":\"getSwVers\",\"enc\":\"base64\",\"payload\":\"UQoAAQYJAAADBAMACcI=\",\"at\":1579783035226,\"parseOk\":true,\"parseResultMessage\":\"Parsed OK\",\"payloadLength\":8,\"flags\":1,\"isReply\":true,\"hardware_revision\":9,\"hardware_revision_MSB\":0,\"hardware_variant\":0,\"hardware_variant_options\":3,\"sw_version_class\":4,\"sw_version_major\":3,\"sw_version_minor\":0,\"sw_version_patch\":9,\"swVersion\":\"3.1.5\",\"receiveAt\":1579783035226}",
        ];
        quePusher(getswv_message);

        setTimeout(async function () {
            hash_1 = await getHash(bemHash, uid_1);
            hash_2 = await getHash(bemHash, uid_2);
            exp_1 = JSON.stringify(hash_1);
            exp_2 = JSON.stringify(hash_2);
            let getswv_return = [
                JSON.stringify({ fwversion: "3.0.9", "status": "connected", "last_up": "2020-01-01T01:01:01Z", "lon": 0.0, "lat": 0.0 }),
                JSON.stringify({ fwversion: "3.1.5", "status": "connected", "last_up": "2020-01-01T01:01:01Z", "lon": 0.0, "lat": 0.0 })
            ];
            assert.equal(exp_1, getswv_return[0]);
            assert.equal(exp_2, getswv_return[1]);
        }, 500);
    });

    it('Updates bems to new fw fom getSwVers messages', async () => {

    });
});

