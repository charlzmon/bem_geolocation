listOfLegacySWVersions = ["2.3.3", "2.3.4"]

//FUNCTIONS//
//Check if fwversion can accept getTower/getLocation requests
function checkFW(fwversion) {
    if (
        fwversion.includes("3.1.") | fwversion.includes("3.2.") | 
        fwversion.includes("3.0.9") | fwversion.includes("3.0.10") |
        listOfLegacySWVersions.includes(fwversion)
        )
        return true;
    else
        return false;
}

let dodgyFW = "3.2.3"

let correctFW = checkFW(dodgyFW)

console.log(correctFW)