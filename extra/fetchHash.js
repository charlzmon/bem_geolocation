const Redis = require('ioredis');
const redis = new Redis();

const bemHash = process.env.BEM_HASH_MAP || "bemStateHash";
const timeoutHash = process.env.TIMEOUT_HASH_MAP || "timeoutHash";

let log = console.log;

async function bemmer() {
    redis.hgetall(bemHash, async (err, object) => {
        if (err) { console.error(err); }
        else {
            for (let key in object) {
                let value = JSON.parse(object[key])
                log({ [key]: value });
            }
        }
    });
}

async function timer() {
    redis.hgetall(timeoutHash, async (err, object) => {
        if (err) { console.error(err); }
        else {
            for (let key in object) {
                let value = object[key]
                log({ [key]: value });
            }
        }
    });
}

bemmer();
timer();

setTimeout(function () { redis.disconnect(); }, 500);