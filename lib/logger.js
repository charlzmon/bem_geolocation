// var winston = require('winston');

// winston.add(winston.transports.File, {
//   filename: './logs/processor.log',
//   level: 'info',
//   json: false,
//   timestamp: function() { return (new Date()).toISOString(); }
// });

// winston.handleExceptions(new winston.transports.File({ filename: './logs/uncaught_processor.log' }));
// winston.prepareForAutomatedDeveloperTesting = function() {
//   winston.remove(winston.transports.Console);
//   winston.remove(winston.transports.File);
//   winston.add(winston.transports.File, { filename: './logs/test.log' });
// };
var path = require('path');



if (!global.hasOwnProperty('__stack')) {
  Object.defineProperty(global, '__stack', {
    get: function () {
      var orig = Error.prepareStackTrace;
      Error.prepareStackTrace = function (_, stack) {
        return stack;
      };
      var err = new Error;
      Error.captureStackTrace(err, arguments.callee);
      var stack = err.stack;
      Error.prepareStackTrace = orig;
      return stack;
    }
  });
}

if (!global.hasOwnProperty('__line')) {
  Object.defineProperty(global, '__line', {
    get: function () {
      return __stack[2].getLineNumber();
    }
  });
}

if (!global.hasOwnProperty('__file')) {
  Object.defineProperty(global, '__file', {
    get: function () {
      var scriptName = path.basename(__stack[2].getFileName());
      return scriptName;
    }
  });
}
//TODO make this a class. Each instance should decide about this.
let usesColors = false;
function noColor() { return usesColors ? '\x1b[0m' : '' };
function red() { return usesColors ? '\x1b[31m' : ''; };// closed with  \x1b[0m
function yellow() { return usesColors ? '\x1b[33m' : ''; };
function blue() { return usesColors ? '\x1b[32m' : ''; };
let logLevel = process.env.BEM_GEOLOCATION_LOGGING_LEVEL || 3;

let fakeLogger = {
  FATAL: 0,
  ERROR: 1,
  WARNING: 2,
  INFO: 3,
  DEBUG: 4,
  TRACE: 5,
  debug: function (val) {
    if (logLevel >= 4) {
      console.log(`${blue()}${(new Date()).toISOString()} DEBUG:(${__file}):${__line}::${val}`);
      // console.log((new Date()).toISOString() + " DEBUG:(" + __file + "):" + __line + ":: " + val);
    }
  },
  info: function (val) {
    if (logLevel >= 3) {
      console.log(`${noColor()}${(new Date()).toISOString()} INFO:("${__file}):${__line}::${val}`);
    }
  },
  warn: function (val) {
    if (logLevel >= 2) {
      // console.log((new Date()).toISOString() + " WARN:(" + __file + "):" + __line + ":: " + val);
      console.log(`${yellow()}${(new Date()).toISOString()} WARN:(${__file}):${__line}::${val}`);
    }
  },
  error: function (val) {
    if (logLevel >= 1) {
      // console.log((new Date()).toISOString() + " ERROR:(" + __file + "):" + __line + ":: " + val);
      console.log(`${red()}${(new Date()).toISOString()} ERROR:(${__file}):${__line}::${val}`);
    }
  },
  setLogLevel: function (level) {
    logLevel = level;
  },
  setUsesColors: function (doesIt) {
    usesColors = doesIt;
  }
};

// module.exports = winston;
module.exports = fakeLogger;

// Winston log levels are: fatal, error, warning, info, debug, trace
