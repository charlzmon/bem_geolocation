const Redis = require("./lib/redisHelper");
const logger = require('./lib/logger');
const IOredis = require('ioredis');
const moment = require('moment');
const cron = require("node-cron");

//Misc
moment().format();
let nowObj = moment(); //Current time
let log = console.log;

//Redis variables
const outQueueName = process.env.ROUTER_OUT_QUEUE || "router_out";
const bemHash = process.env.BEM_HASH_MAP || "bemStateHash";
const timeoutHash = process.env.TIMEOUT_HASH_MAP || "timeoutHash";
//Redis instances
var redisPort = process.env.REDIS_PORT || 6379;
var redisHost = process.env.REDIS_HOST || "localhost";
const pushCli = new Redis(redisPort, redisHost);
const redis = new IOredis();

//List of software versions that can accept a get tower info request
listOfSWVersions = [
    "3.0.9", "3.0.10", "3.1.0", "3.1.1", "3.1.2", "3.1.3", "3.1.4", "3.1.5"
];


function print(val) { log(); log(val); log(); }

//Get the timeout for UID
async function getTimeout(UID) {
    try {
        let reply = await redis.hmget(timeoutHash, UID);
        let parsed = JSON.parse(reply[0]);
        return parsed;
    } catch{ log(err); log(error); log(Error); }
}

//Queue message used for getTower and setLocation
function queueMessage(UID, messageType, payload, callback) {
    print("Making a " + messageType + " request for " + UID);
    let message = { UID: UID, type: messageType, direction: 'down', };
    if (payload === undefined) {
        message.payload = 'none';
    }
    else message = Object.assign(message, payload);
    let messageStr = JSON.stringify(message);
    logger.info(`Sending *** ${messageStr} *** to ${outQueueName}`);
    pushCli.queueMessage(outQueueName, messageStr, callback);
}

// Execute every hour
cron.schedule("1 * * * *", function () {
    log("Started cronjob");
    redis.hgetall(bemHash, async (err, object) => {//Retrieve all objects in hash
        if (err) { console.error(err); }
        else {
            for (let key in object) { //Loop through all object values
                let value = JSON.parse(object[key]);
                let bemIsConnected = (value.status == "connected");
                let correctFW = listOfSWVersions.includes(value.fwversion);

                if (bemIsConnected && correctFW) {
                    let last_up = moment(value.last_up);
                    let duration = moment.duration(nowObj.diff(last_up));
                    let hours = duration.asHours(); //Time since last location update in hours
                    let timeout = await getTimeout(key);

                    if (hours > timeout) {
                        queueMessage(key, "getTowerInfo");
                    }
                }
            }
        }
    });
});