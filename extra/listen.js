const Redis = require("./lib/redisHelper");
const logger = require('./lib/logger');
const IOredis = require('ioredis');
const rp = require('request-promise');
const moment = require('moment');

//Misc
moment().format();
let nowObj = moment(); //Current time
let log = console.log;

//Redis variables
const inQueueName = process.env.LOCATION_EVENT_QUEUE || "locationQueue";
const outQueueName = process.env.ROUTER_OUT_QUEUE || "router_out";
const bemHash = process.env.BEM_HASH_MAP || "bemStateHash";
const timeoutHash = process.env.TIMEOUT_HASH_MAP || "timeoutHash";
//Redis instances
const redisPort = process.env.REDIS_PORT || 6379;
const redisHost = process.env.REDIS_HOST || "localhost";
const pushCli = new Redis(redisPort, redisHost);
const lisCli = new Redis(redisPort, redisHost);
const redis = new IOredis();

//List of software versions that can accept a get tower info request
listOfSWVersions = [
    "3.0.9", "3.0.10", "3.1.0", "3.1.1", "3.1.2", "3.1.3", "3.1.4", "3.1.5"
];


//FUNCTIONS//
function print(val) { log(); log(val); log(); }

//Queue message used for getTower and setLocation
function queueMessage(UID, messageType, payload, callback) {
    print("Making a " + messageType + " request for " + UID);
    let message = { UID: UID, type: messageType, direction: 'down', };
    if (payload === undefined) {
        message.payload = 'none';
    }
    else {
        message = Object.assign(message, payload);
    }
    let messageStr = JSON.stringify(message);
    logger.info(`Sending *** ${messageStr} *** to ${outQueueName}`);
    pushCli.queueMessage(outQueueName, messageStr, callback);
}

//Coordinates lookup service
async function coordinatesLookup(tower) {
    print("Coordinates lookup for: \n" + JSON.stringify(tower));
    let towerInfo = {
        method: 'POST',
        uri: 'https://ap1.unwiredlabs.com/v2/process.php',
        body: {
            "token": "94b5a55213320c", //94b5a55213320c //9feb2a1b25d727
            "radio": "gsm",
            "mcc": tower[0].mcc,
            "mnc": tower[0].mnc,
            "cells": tower,
            "address": 1
        }, json: true
    }
    let reply = await rp(towerInfo);
    print(reply);
    logger.info(`Lookup Reply: ${reply}`);
    let coordinates = { lon: reply.lon, lat: reply.lat, status: reply.status };
    return coordinates;
}

//Retrieve hash map from Redis
async function getHash(UID, key) {
    try {
        let reply = await redis.hmget(key, UID);
        let parsed = JSON.parse(reply[0]);
        return parsed;
    } catch{ print(err); }
}

//Update the hash map
async function setHash(UID, bem) {
    let json = { [UID]: JSON.stringify(bem) };
    redis.hmset(bemHash, json, (err, reply) => {
        if (err) return "Error";
        else return reply;
    });
}

//Create a new bem object to be stored in hash map
function makeHash(now) {
    newBem = {
        "status": "connected",
        "last_up": now,
        "lon": 0.0,
        "lat": 0.0
    }
    return newBem;
}


//LISTEN FOR NEW EVENTS ON QUEUE//
lisCli.startListeningOnQueue(inQueueName, async (eventMessage) => {
    log();
    logger.info(`Message Received: ${eventMessage}`);
    let message = JSON.parse(eventMessage);
    let eventUID = message.UID;
    let eventType = message.type;
    let now = nowObj.toISOString();
    let bemExists = await redis.hexists(bemHash, eventUID); //If UID is in hasmap
    //Check the event type
    switch (eventType) {
        //If the incoming event is a statusUpdate event
        case "statusUpdate": {
            let connectEvent = message.status == "CONNECTED";
            if (bemExists) {
                let bem = await getHash(eventUID, bemHash);
                let bemIsConnected = (bem.status == "connected");
                //If a "CLOSED" event is found for a connected bem               
                if (bemIsConnected && !connectEvent) {
                    // Update to "disconnected" status
                    bem.status = "disconnected";
                    setHash(eventUID, bem);
                }
                //If a "CONNECTED" event is found for a disconnected bem
                else if (!bemIsConnected && connectEvent) {
                    //Make get getSwVers request on every connected event
                    queueMessage(eventUID, "getSwVers");
                    // Update to "connected" status
                    bem.status = "connected";
                    setHash(eventUID, bem);
                    //If BEM has fw that can accept GetTowerInfo requests
                    let correctFW = listOfSWVersions.includes(bem.fwversion);
                    if (correctFW) {
                        // Make GetTowerInfo request if enough...  
                        //...time has passed since last connected
                        let last_up = moment(bem.last_up);
                        let duration = moment.duration(nowObj.diff(last_up));
                        let hours = duration.asHours();
                        let timeout = await getHash(eventUID, timeoutHash);
                        if (hours > timeout) queueMessage(eventUID, "getTowerInfo");
                    }
                }
            } else {
                queueMessage(eventUID, "getSwVers");
            }
            break;
        }
        //If the incoming event is a Get tower info reply
        case "getTowerInfo": {
            if (bemExists) {
                let bem = await getHash(eventUID, bemHash);
                let bemIsConnected = (bem.status == "connected");

                if (bemIsConnected) {
                    //Attempt a coordinates lookup from tower info
                    let tower = message.towerInfo;
                    let coordinates = await coordinatesLookup(tower);

                    if (coordinates.status == 'ok') {
                        //Update hashmap
                        bem.lon = coordinates.lon;
                        bem.lat = coordinates.lat;
                        bem.last_up = now;
                        setHash(eventUID, bem);
                        //Make Set_Location request
                        let payload = {
                            latitude: coordinates.lat * 1000000,
                            longitude: coordinates.lon * 1000000
                        };
                        queueMessage(eventUID, "setLocation", payload);
                        queueMessage(eventUID, "setLocation", payload);
                        queueMessage(eventUID, "setLocation", payload);
                        queueMessage(eventUID, "setLocation", payload);
                    }
                }
            }
            break;
        }
        //If the incoming event is a SetLocation reply
        case "setLocation": {
            if (bemExists) {
                let bem = await getHash(eventUID, bemHash);
                let successful = false;
                if (successful) {
                    setHash(eventUID, bem);
                }
            }
            break;
        }
        //If the incoming event is a GetSwVersion reply 
        case "getSwVers": {
            if (bemExists) {
                let bem = await getHash(eventUID, bemHash);
                bem.fwversion = message.swVersion;
                setHash(eventUID, bem);
            } else {
                let bem = await makeHash(now);
                queueMessage(eventUID, "getTowerInfo");
                setHash(eventUID, bem);
            }
            break;
        }
    }
});