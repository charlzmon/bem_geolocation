const Redis = require("./lib/redisHelper");
const logger = require('./lib/logger');
const IOredis = require('ioredis');
const rp = require('request-promise');
const moment = require('moment');
const cron = require("node-cron");

//Misc
moment().format();
let log = console.log;
//function print(val) { logger.debug(); logger.debug(val); logger.debug(); }

//Redis variables
const inQueueName = process.env.LOCATION_EVENT_QUEUE || "locationQueue";
const outQueueName = process.env.ROUTER_OUT_QUEUE || "router_out";
const bemHash = process.env.BEM_HASH_MAP || "bemStateHash";
const timeoutHash = process.env.TIMEOUT_HASH_MAP || "timeoutHash";
const stationaryTimeOut = process.env.STATIONARY_TIMEOUT_DEFAULT || 3
const token = process.env.LOCATION_LOOKUP_TOKEN || "94b5a55213320c"; //94b5a55213320c //9feb2a1b25d727

//Redis instances
const redisPort = process.env.REDIS_PORT || 6379;
const redisHost = process.env.REDIS_HOST || "localhost";
const pushCli = new Redis(redisPort, redisHost);
const lisCli = new Redis(redisPort, redisHost);
const redis = new IOredis(redisPort, redisHost);

//List of software versions that can only receive getLocation request (as opposed to getTower request)
listOfLegacySWVersions = ["2.3.3", "2.3.4", "2.3.5"]

//FUNCTIONS//
//Check if fwversion can accept getTower/getLocation requests
function checkFW(fwversion) {
    if (
        fwversion.includes("3.1.") | fwversion.includes("3.2.") |
        fwversion.includes("3.0.9") | fwversion.includes("3.0.10") |
        listOfLegacySWVersions.includes(fwversion)
    )
        return true;
    else
        return false;
}

//Queue message used for getTower and setLocation
function queueMessage(UID, messageType, payload, callback) {
    let message = { UID: UID, type: messageType, direction: 'down' };
    if (payload === undefined) {
        message.payload = 'none';
    }
    else {
        message = Object.assign(message, payload);
    }
    let messageStr = JSON.stringify(message);
    logger.info(`Sending *** ${messageStr} *** to ${outQueueName}`);
    pushCli.queueMessage(outQueueName, messageStr, callback);
}

function requestTowerinfo(uid, fw, source) {
    if (listOfLegacySWVersions.includes(fw)) {
        logger.info(`Making a getLocation request for: ${uid}, initiated by ${source}`);
        queueMessage(uid, "getLocation");
    }
    else {
        logger.info(`Making a getTowerInfo request for: ${uid}, initiated by ${source}`);
        queueMessage(uid, "getTowerInfo");
    }
    //return reply
}

//Coordinates lookup service
async function coordinatesLookup(tower, uid) {
    logger.info(`Coordinates lookup requested for ${uid}: ${JSON.stringify(tower)}`);
    let towerInfo = {
        method: 'POST',
        uri: 'https://eu1.unwiredlabs.com/v2/process.php',
        body: {
            "token": token, //94b5a55213320c //9feb2a1b25d727
            "id":uid,
            "radio": "gsm",
            "mcc": tower[0].mcc,
            "mnc": tower[0].mnc,
            "cells": tower,
            "address": 1
        }, json: true
    }
    let reply = await rp(towerInfo);
    logger.info(`Lookup reply for ${uid}: ${JSON.stringify(reply)}`);
    return reply
}

//Retrieve hash map from Redis
async function getHash(key, UID) {
    try {
        let reply = await redis.hmget(key, UID);
        let parsed = JSON.parse(reply[0]);
        return parsed;
    } catch { logger.error(err); }
}

//Update the hash map
async function setHash(UID, bem) {
    let json = { [UID]: JSON.stringify(bem) };
    redis.hmset(bemHash, json, (err, reply) => {
        if (err) {
            logger.error(err);
        } else {
            logger.info(`Setting hash map for ${JSON.stringify(json)} :: ${reply}`);
        }
    });
}

//Create a new bem object to be stored in hash map
function makeHash(fwversion) {
    newBem = {
        "fwversion": fwversion,
        "status": "connected",
        "last_up": "2020-01-01T01:01:01Z",
        "lon": 0.0,
        "lat": 0.0
    }
    logger.info(`New BEM added to hash map: ${JSON.stringify(newBem)}`);
    return newBem;
}

//LISTEN FOR NEW EVENTS ON QUEUE//
lisCli.startListeningOnQueue(inQueueName, async (eventMessage) => {
    logger.info(`Message Received: ${eventMessage}`);
    let message = JSON.parse(eventMessage);
    let eventUID = message.UID;
    let eventType = message.type;
    let bemExists = await redis.hexists(bemHash, eventUID); //If UID is in hasmap
    //Check the event type
    //If the incoming event is a statusUpdate event
    if (eventType == "statusUpdate") {
        let connectEvent = message.status == "CONNECTED";
        if (bemExists) {
            let bem = await getHash(bemHash, eventUID);
            let bemIsConnected = (bem.status == "connected");
            //If a "CLOSED" event is found for a connected bem               
            if (bemIsConnected && !connectEvent) {
                bem.status = "disconnected";    // Update to "disconnected" status
                setHash(eventUID, bem);
            }
            //If a "CONNECTED" event is found for a disconnected bem
            else if (!bemIsConnected && connectEvent) {
                queueMessage(eventUID, "getSwVers");    //Make getSwVers request on every connected event
                bem.status = "connected";   // Update to "connected" status
                setHash(eventUID, bem);
                let correctFW = checkFW(bem.fwversion)
                if (correctFW) {    //If BEM has fw that can accept GetTowerInfo requests
                    // Make GetTowerInfo request if enough...  
                    //...time has passed since last connected
                    let last_up = moment(bem.last_up);
                    let nowObj = moment();
                    let duration = moment.duration(nowObj.diff(last_up));
                    let hours = duration.asHours();
                    let timeout = await getHash(timeoutHash, eventUID);
                    if (hours > timeout) {
                        //queueMessage(eventUID, "getTowerInfo");
                        requestTowerinfo(eventUID, bem.fwversion, eventType);
                    }
                }
            }
        } else {
            queueMessage(eventUID, "getSwVers");
        }

    }
    //If the incoming event is a Get tower info reply
    else if ((eventType == "getTowerInfo") || (eventType == "getLocation")) {
        if (bemExists) {
            let bem = await getHash(bemHash, eventUID);
            let bemIsConnected = (bem.status == "connected");

            if (bemIsConnected) {
                //Attempt a coordinates lookup from tower info
                let tower = message.towerInfo;
                let coordinates = await coordinatesLookup(tower, eventUID);

                if (coordinates.status == 'ok') {
                    //Update hashmap
                    bem.lat = coordinates.lat;
                    bem.lon = coordinates.lon;
                    let nowObj = moment();
                    let now = nowObj.toISOString();
                    bem.last_up = now;
                    setHash(eventUID, bem);
                    //Make Set_Location request
                    let payload = {
                        latitude: coordinates.lat * 1000000,
                        longitude: coordinates.lon * 1000000
                    };
                    queueMessage(eventUID, "setLocation", payload);

                } else if (coordinates.message == 'No matches found') {
                    //Delay next lookup by one day to save lookups
                    let nowObj = moment();
                    let new_date = moment(nowObj).add(1, 'days');
                    bem.last_up = new_date.toISOString();
                    setHash(eventUID, bem);
                }
            }
        } else {
            queueMessage(eventUID, "getSwVers");
        }
    }
    //If the incoming event is a GetSwVersion reply 
    else if (eventType == "getSwVers") {
        if (bemExists) {
            let bem = await getHash(bemHash, eventUID);
            bem.fwversion = message.swVersion;
            setHash(eventUID, bem);
        } else {
            let bem = await makeHash(message.swVersion);
            logger.info(`Adding ${eventUID} to hashmap: ${JSON.stringify(bem)}`);
            let correctFW = checkFW(bem.fwversion)
            if (correctFW) {
                //queueMessage(eventUID, "getTowerInfo");
                requestTowerinfo(eventUID, bem.fwversion, 'new hashmap entry');
            }
            let timeoutVal = { [eventUID]: [stationaryTimeOut] };
            redis.hmset(timeoutHash, timeoutVal, (err, reply) => {
                if (err) return "Error";
                else return reply;
            });
            setHash(eventUID, bem);
        }
    }
});


// EXECUTE 10 MINUTES
cron.schedule("*/10 * * * *", function () {
    let nowObj = moment(); //Current time
    logger.info("Started cronjob");
    //Retrieve all objects in hash
    redis.hgetall(bemHash, async (err, object) => {
        if (err) {
            logger.error(err);
        }
        else {
            //Loop through all object values
            for (let key in object) {
                let value = JSON.parse(object[key]);
                let bemIsConnected = (value.status == "connected");
                let correctFW = checkFW(value.fwversion)
                if (bemIsConnected && correctFW) {
                    let last_up = moment(value.last_up);
                    let duration = moment.duration(nowObj.diff(last_up));
                    let hours = duration.asHours();
                    let timeout = await getHash(timeoutHash, key);
                    if (hours > timeout) {
                        requestTowerinfo(key, value.fwversion, 'cron job');
                        /*
                        if (listOfLegacySWVersions.includes(value.fwversion)) {
                            logger.info(`Making a getLocation request for: ${key} initiated by cron`);
                            queueMessage(key, "getLocation");
                        }
                        else {
                            logger.info(`Making a getTowerInfo request for: ${key} initiated by cron`);
                            queueMessage(key, "getTowerInfo");
                        }
                        */
                    }
                }
            }
        }
    });
});



exports.getHash = getHash;