const logger = require('./logger');
const redis = require('redis');
//import logger from './logger';

//var RedisClassVar = class RedisHelper {
class RedisHelper {
  /* Create a new one of these per redis client that is to be created. */
  constructor(port, hostname) {
    this.isListening = false;
    this.listeningTo = null;
    this.redisClient = redis.createClient(port, hostname); //TODO throw error if this is not successful. Does from below...
    logger.info(`redis client created with port: ${port} and host:${hostname}`);
  }

  /*
   * Put a string message onto a particular queue using a particular redis-client.
   */
  queueMessage(queue, message, callback) {
    if (this.isListening) {
      logger.error(`Blocking client used for non-blocking call`);
      throw new Error('ERROR non-blocking call');
    }
    if (typeof (message) !== 'string') {
      message = JSON.stringify(message);
      logger.error(`Messages to be queued must be strings: ${message}`);
    }
    this.redisClient.lpush(queue, message, function (err) {
      if (err) {
        logger.error(`lpush onto : ${queue}, data: ${message}`);
        throw new Error(`ERROR while lpush onto : ${queue}, data: ${message}`);
      } else if (callback) {
        callback();
      }
    });
  }

  /*
   * pop a string message off a particular queue using a particular redis-client.
   */
  deQueueMessage(queue, callback) {
    if (this.isListening) {
      logger.error(`Blocking client used for non-blocking call`);
      throw new Error('ERROR non-blocking call');
    }
    this.redisClient.rpop(queue, function (err, message) {
      if (err) {
        logger.error(`lpop off: ${queue}`);
        throw new Error(`ERROR while rpop off : ${queue}`);
      } else if (message) {
        logger.info(`Message popped off ${queue}: ${message}`);
        callback(message);
      } else {
        logger.info(`Queue: ${queue} empty`);
        callback(null);
      }
    });
  }

  /*
   * check that a particular queue has data using a particular redis-client.
   */
  queueExists(queue, callback) {
    if (this.isListening) {
      logger.error(`Blocking client used for non-blocking call`);
      throw new Error('ERROR non-blocking call');
    }
    this.redisClient.llen(queue, function (err, length) {
      if (err) {
        logger.error(`llen of : ${queue}`);
        throw new Error(`ERROR while llen of : ${queue}`);
      } else if (length > 0) {
        logger.info(`llen of ${queue} is ${length}`);
        callback(true);
      } else {
        logger.info(`llen of ${queue} is ${length}`);
        callback(false);
      }
    });
  }

  //test >> need to add the rest of the set control functions to this.
  setHasMember(set, member, callback) {
    if (this.isListening) {
      logger.error(`Blocking client used for non-blocking call`);
      throw new Error('ERROR non-blocking call');
    }
    this.redisClient.sismember(set, member, function (err, isMember) {
      if (err) {
        logger.error(`sismember of : ${set}`);
        throw new Error(`ERROR while sismember of : ${set}`);
      } else if (isMember == 1) {
        logger.info(`${member} is a member of${set}`);
        callback(true);
      } else {
        logger.info(`${member} is NOT a member of : ${set}, ${err}`);
        callback(false);
      }
    });
  }

  setAddMember(set, member, callback) {
    if (this.isListening) {
      logger.error(`Blocking client used for non-blocking call`);
      throw new Error('ERROR non-blocking call');
    }
    this.redisClient.sadd(set, member, (err, num) => {
      if (err) {
        logger.error(`Cannot add member to: ${set}`);
        throw new Error(`ERROR while sadd of : ${set}, ${err}`);
      } else {
        if (callback) callback(num);
      }
    });
  }

  setRemoveMember(set, member, callback) {
    if (this.isListening) {
      logger.error(`Blocking client used for non-blocking call`);
      throw new Error('ERROR non-blocking call');
    }
    this.redisClient.srem(set, member, (err, num) => {
      if (err) {
        logger.error(`Cannot remove member from: ${set}`);
        throw new Error(`ERROR while srem of : ${set}, ${err}`);
      } else {
        if (callback) callback(num);
      }
    });
  }

  //TODO add: hdel hget hexists hset hsetnx sadd

  /**
   * Create a blocking pop chain, waiting on messages from a particular queue-name.
   * Ensure that this redis-client is reserved for this purpose.
   */
  startListeningOnQueue(queueName, callback) {
    if (!this.isListening) {
      this.isListening = true;
      this.listeningTo = queueName;
      if (this.redisClient) {
        logger.info(`redis listening to ${queueName}`);
        brpopQueue(this, queueName, callback);
      }
    } else {
      throw new Error('ERROR blocking call repeated');
    }
  }

  /** TODO remove */
  stopListening() {
    // logger.warn("Deprecated, will be removed. Use close instead.")
    this.isListening = false; // this.close(); // Stops chaining new blocking reads.
  }

  close(cb) {
    this.isListening = false;
    this.redisClient.quit(() => { if (cb) cb(); });
  }
}

/**
 * Keep calling the blocking pop chain, waiting on messages from a particular queue-name.
 * Ensure that this redis-client is reserved for this purpose.
 */
const brpopQueue = function bQ(helper, queueName, callback) {
  helper.redisClient.brpop(queueName, 0, function brpopCB(err, data) {
    if (err) {
      logger.error(`lpush onto : ${queueName}: ${err}`);
      throw new Error(`ERROR while popping: ${queueName}: ${err}`);
    }
    logger.info(`item popped off ${queueName}, item: ${data[1]} with type ${typeof data[1]}`);
    var data_no_newlines = data[1].replace(/(\t|\n|\r)/gm, "");
    callback(data_no_newlines); // this was  (`{${data[1]}}`);(`{${data[1]}}`);
    if (helper.isListening)
      brpopQueue(helper, queueName, callback);
  });
};

//export default RedisHelper;
module.exports = RedisHelper;
//exports.RedisHelper = RedisClassVar;
